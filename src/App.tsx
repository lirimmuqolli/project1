/** @format */

import { useEffect, useState } from "react";
import axios from "axios";
import "./App.css";

interface Product {
  id: number;
  thumbnail: string;
  title: string;
}

function App(): JSX.Element {
  const [products, setProducts] = useState<Product[]>([]);
  const [page, setPage] = useState<number>(1);

  const fetchProducts = async (): Promise<void> => {
    try {
      const res = await axios.get("https://dummyjson.com/products?limit=100");
      const data: { products: Product[] } = res.data;

      console.log(data);

      if (data && data.products) {
        setProducts(data.products);
      }
    } catch (error) {
      console.error("Error fetching products:", error);
    }
  };

  useEffect(() => {
    fetchProducts();
  }, []);

  const selectPageHandler = (selectedPage: number): void => {
    if (
      selectedPage >= 1 &&
      selectedPage <= Math.ceil(products.length / 10) &&
      selectedPage !== page
    ) {
      setPage(selectedPage);
    }
  };

  return (
    <div>
      {products.length > 0 && (
        <div className="products d-flex flex-wrap">
          {products.slice(page * 10 - 10, page * 10).map((prod: Product) => (
            <span className="products__single" key={prod.id}>
              <img src={prod.thumbnail} alt={prod.title} /> {/* alt is imp */}
              <span>{prod.title}</span>
            </span>
          ))}
        </div>
      )}

      {products.length > 0 && (
        <div className="pagination">
          <span
            onClick={() => selectPageHandler(page - 1)}
            className={page > 1 ? "" : "pagination__disable"}
          >
            ◀
          </span>

          {[...Array(Math.ceil(products.length / 10))].map((_, i) => (
            <span
              key={i}
              className={page === i + 1 ? "pagination__selected" : ""}
              onClick={() => selectPageHandler(i + 1)}
            >
              {i + 1}
            </span>
          ))}

          <span
            onClick={() => selectPageHandler(page + 1)}
            className={
              page < Math.ceil(products.length / 10)
                ? ""
                : "pagination__disable"
            }
          >
            ▶
          </span>
        </div>
      )}
    </div>
  );
}

export default App;
